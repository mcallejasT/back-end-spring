package com.technical.test.java.birthdayapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.technical.test.java.birthdayapp.model.BirthdayData;
import com.technical.test.java.birthdayapp.model.PoemData;
import com.technical.test.java.birthdayapp.util.UtilBirthday;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class BirthdayController {
	
	private static final Logger logger = LoggerFactory.getLogger(BirthdayController.class);
	
	 @GetMapping("/api/getBirthday")
	    public BirthdayData  getBirthdayData(BirthdayData params) {
	    	
		 	logger.info("Consulta: " + params);
	    	
		 	params = UtilBirthday.getDaysTillBirthday(params);
		 	Long age = UtilBirthday.calculateAge(params.getBirthDate());
		 	params.setAge(age);
		 	
		 	if(params.getDaysTillBirthday() == 0) {
		 		params = getPoem(params);
		 	}
		 	
		 	params = UtilBirthday.getNames(params);
		 	
	    	return params;
	    }

	 
	 private BirthdayData getPoem(BirthdayData params) {
		 
	    	RestTemplateBuilder builder = new RestTemplateBuilder();
	    	RestTemplate restTemplate = builder.build();
	    	
			PoemData[] poem = restTemplate.getForObject("https://www.poemist.com/api/v1/randompoems", PoemData[].class);
			logger.info("first poem: " +  poem[1].toString());
			
			params.setPoem(poem[0]);
		 
		 return params;
	 }
		
}
