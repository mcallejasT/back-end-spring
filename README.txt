Back End Spring Boot

Aplicación Rest construido como proyecto Gradle con Spring Boot 2.3.0 y Java 8

URL: http://localhost:8081/birthday-rest/api/getBirthday?fullName=Nombre%20Apellido&birthDate=01-01-2000

Repositorio Git
https://mcallejasT@bitbucket.org/mcallejasT/back-end-spring.git

Instrucciones de Despliegue:

1) Prerrequisitos: Apache tomcat
2) Descargar las fuentes del repositorio Git
3) Generar el archivo .war mediante el comando ./gradlew war
4) El archivo será generado en la carpeta build/libs/
5) Modificar el nombre del war generado a birthday-rest.war
6) Desplegar el war generado en el servidor TOMCAT (manager-apps)
7) Probar despliegue llamando a la URL descrita reemplazando el host y el puerto según corresponda.
     




