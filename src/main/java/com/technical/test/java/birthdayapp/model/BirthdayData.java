package com.technical.test.java.birthdayapp.model;

import java.io.Serializable;

public class BirthdayData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7096612959452260391L;
	private String fullName;
	private String name;
	private String surname;
	private Long age;
	private String birthDate;
	private String NewBirthDate;
	private Long daysTillBirthday;
	private PoemData poem;
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Long getAge() {
		return age;
	}
	public void setAge(Long age) {
		this.age = age;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getNewBirthDate() {
		return NewBirthDate;
	}
	public void setNewBirthDate(String newBirthDate) {
		NewBirthDate = newBirthDate;
	}
	public Long getDaysTillBirthday() {
		return daysTillBirthday;
	}
	public void setDaysTillBirthday(Long daysTillBirthday) {
		this.daysTillBirthday = daysTillBirthday;
	}
	public PoemData getPoem() {
		return poem;
	}
	public void setPoem(PoemData poem) {
		this.poem = poem;
	}
	@Override
	public String toString() {
		return "BirthdayData [fullName=" + fullName + ", name=" + name + ", surname=" + surname + ", age=" + age
				+ ", birthDate=" + birthDate + ", NewBirthDate=" + NewBirthDate + ", daysTillBirthday="
				+ daysTillBirthday + ", poem=" + poem + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((NewBirthDate == null) ? 0 : NewBirthDate.hashCode());
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((daysTillBirthday == null) ? 0 : daysTillBirthday.hashCode());
		result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((poem == null) ? 0 : poem.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BirthdayData other = (BirthdayData) obj;
		if (NewBirthDate == null) {
			if (other.NewBirthDate != null)
				return false;
		} else if (!NewBirthDate.equals(other.NewBirthDate))
			return false;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (daysTillBirthday == null) {
			if (other.daysTillBirthday != null)
				return false;
		} else if (!daysTillBirthday.equals(other.daysTillBirthday))
			return false;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals(other.fullName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (poem == null) {
			if (other.poem != null)
				return false;
		} else if (!poem.equals(other.poem))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

}
