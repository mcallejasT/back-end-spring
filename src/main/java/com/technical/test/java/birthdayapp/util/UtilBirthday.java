package com.technical.test.java.birthdayapp.util;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.technical.test.java.birthdayapp.model.BirthdayData;

public class UtilBirthday {
	
	private static final Logger logger = LoggerFactory.getLogger(UtilBirthday.class);

	
	 public static BirthdayData getDaysTillBirthday(BirthdayData data) {
		 		 		 
		LocalDate dateBefore = LocalDate.parse(getDate(data.getBirthDate()));
		LocalDate now = LocalDate.now();
		
		int dayOfMonth = dateBefore.getDayOfMonth();
		Month month = dateBefore.getMonth();
		int year = now.getYear();
	
		LocalDate newDateBefore = LocalDate.of(year, month, dayOfMonth);
		
		if(newDateBefore.isBefore(now)) {
			newDateBefore = newDateBefore.plusYears(1);
		}
		
		data.setDaysTillBirthday(ChronoUnit.DAYS.between(now, newDateBefore));
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
		data.setNewBirthDate(dateBefore.format(formatter));
		 
		logger.info("[getDaysToBirthday] data: " + data);
		
		 return data;
	 }
	
	 
	 public static long calculateAge(String BirthDate) {
		 
		LocalDate dateBefore = LocalDate.parse(getDate(BirthDate));
		LocalDate dateAfter = LocalDate.now();
			 
		long age = ChronoUnit.YEARS.between(dateBefore, dateAfter);
			 
		logger.info("[calculateAge] age: " + age);
			
		return age;  
	 }
	 
	 public static BirthdayData getNames(BirthdayData data) {
		 
		 String fullName = data.getFullName();
		 String[] fullNameArray = fullName.split("\\s+");
		 
		 data.setName(fullNameArray[0]);
		 
		 if(fullNameArray.length > 2) {
			 data.setSurname(fullNameArray[2]);
		 }else if(fullNameArray.length > 1){
			 data.setSurname(fullNameArray[1]);
		 }
		 
		 logger.info("[getNames] data: " + data);
		 
		 return data;
	 }
	 
	 private static String getDate(String sDate) {
		 
		 String[] splittedDate = sDate.split("-");
		 String newDate = splittedDate[2] + "-" + splittedDate[1] + "-" +splittedDate[0];
		 return newDate;
	 }
}
